let Account = require("../../models/Account");

exports.account_list = function(req,res) {
    Account.allAccounts(function (err, accounts) {
        if (err) res.send(500, err.message);
        res.status(200).json({
            accounts: accounts
        });
    });
};

exports.account_create = function(req,res) {
    let account = new Account({
        name: req.body.name,
        balance: req.body.balance
    });

    Account.add(account, function (err, account) {
        if (err) res.send(500, err.message);
        res.status(201).json({ // Codigo 201 == CREATE con éxito
            account: account
        })
    });
};

exports.account_delete = function(req,res){
    let account = req.body._id;

    Account.removeById(account, function (err, account) {
        if (err) res.send(500, err.message);
        res.status(204).send();
    });
}

exports.account_update = function(req,res){
    Account.findById(req.body._id, function (err, account) {
        if (err) res.send(500, err.message);
        
        let modified = {
            name: req.body.name,
            balance: req.body.balance
        }
        Account.update({_id: account._id}, modified, function (error, doc) {
            if (error) res.send(500, err.message);
            res.status(200).send();
        });
    });
}