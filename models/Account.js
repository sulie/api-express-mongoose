let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let accountSchema = new Schema({
    name: String,
    balance: Number
});

accountSchema.statics.allAccounts = function (cb) {
    return this.find({}, cb);
}

accountSchema.statics.add = function (aAccount, cb) {
    return this.create(aAccount, cb);
}

accountSchema.statics.findById = function (id, cb) {
    return this.findOne({_id: id}, cb);
}

accountSchema.statics.removeById = function (id, cb) {
    return this.deleteOne({_id: id}, cb);
}

module.exports = mongoose.model("Account", accountSchema);