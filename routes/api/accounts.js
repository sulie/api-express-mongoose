let express = require('express');
let router = express.Router();
let accountControllerAPI = require("../../controllers/api/accountControllerAPI");

router.get("/", accountControllerAPI.account_list);
router.post("/create", accountControllerAPI.account_create);
router.delete("/delete", accountControllerAPI.account_delete);
router.put("/update", accountControllerAPI.account_update);
module.exports = router;